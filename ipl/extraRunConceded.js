const extraRunConceded = (deliveries, matches) => {
  const matchesId =  matches.filter((eachMatch) => eachMatch.season === "2016").map((eachMatch) => eachMatch.id);

  const extraRuns = deliveries.reduce((acc, delivery) => {
    if (matchesId.includes(delivery.match_id)) {
      const extraRun = delivery.extra_runs;
      const team = delivery.bowling_team;

      acc[team] = acc[team]
        ? (acc[team] += parseInt(extraRun))
        : parseInt(extraRun);
    }
    return acc;
  }, {});

  return extraRuns;
};
module.exports = extraRunConceded;
