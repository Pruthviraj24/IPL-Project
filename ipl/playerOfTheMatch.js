const playerOfTheMatch = (matches) => {
  const yearWisePlayerOfMatch = matches.reduce((acc, curr) => {
    if (acc[curr.season]) {
      if (acc[curr.season][curr.player_of_match]) {
        acc[curr.season][curr.player_of_match]++;
      } else {
        acc[curr.season][curr.player_of_match] = 1;
      }
    } else {
      acc[curr.season] = {};
      acc[curr.season][curr.player_of_match] = 1;
    }
    return acc;
  }, {});

  const palyerOfTheMatchEachYear = Object.entries(yearWisePlayerOfMatch).reduce(
    (acc, eachYear) => {
      const temp = Object.entries(eachYear[1])
        .sort(([, a], [, b]) => [, b][1] - [, a][1])
        .slice(0, 1);
      acc[eachYear[0]] = temp[0];
      return acc;
    },
    {}
  );

  return palyerOfTheMatchEachYear;
};

module.exports = playerOfTheMatch;
